##########################################################################################
# SYNTAX: python PileupParser.py pileup_input_file output_file                           #
##########################################################################################

import re, sys

readStartPattern    = re.compile("(\^.)")
insertPattern       = re.compile("(\+)([\d]+)([ACGTNacgtn]+)")
deletePattern       = re.compile("(-)([\d]+)([ACGTNacgtn]+)")

def ProcessString(string):
    toRemove = ["$"]
    for i in readStartPattern.findall(string):
        toRemove.append(i)
    for match in (insertPattern.findall(string) + deletePattern.findall(string)):
        match = list(match)
        match[2] = match[2][:int(match[1])]
        toRemove.append("".join(match))
    for i in toRemove:
        string = string.replace(i, "")
    return string

def EnumerateBases(string):
    baseFrequency = {"A" : 0, "T" : 0, "G" : 0, "C" : 0}
    for base in string:
        try:
            baseFrequency[base.upper()] += 1
        except KeyError:
            continue
    return "\t".join("%s:%i" % (key, value) for key, value in baseFrequency.items())

with open(sys.argv[2], "w") as outputHandle:
    for line in open(sys.argv[1], "rU").readlines():
        _line = line.strip().split("\t")
        outputHandle.write("%s\t%s\n" % ("\t".join(_line[:4]), EnumerateBases(ProcessString(_line[4]))))