Pileup Parser
=============
Scripts to parse SAM pileup dumps.  

Project Correspondents:
-----------------------
Dr. Chris Baysdorfer  
California State University, East Bay