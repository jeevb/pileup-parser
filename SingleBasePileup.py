##########################################################################################
# SYNTAX: python PileupParserExtended.py pileup_input_file output_file                   #
##########################################################################################

import os, re, sys, operator

##########################################################################################
# C O N F I G U R A T I O N                                                              #
##########################################################################################
readStartPattern            = re.compile("(\^.)")
indelPattern                = re.compile("([\+-])([\d]+)([ACGTNacgtn]+)")
BASE_COORD                  = [1505, 1506, 1507, 1583, 1584, 1585, 1616, 1617, 1662, 1663]
##########################################################################################

class PileupParser():

    def ProcessRecord(self, _record):
        record = _record.strip().split("\t")
        if BASE_COORD and int(record[1]) not in BASE_COORD:
            return ""
        stringSummary = self.EnumerateBases(self.ProcessString(record[4]))
        return "%s\t%s\n" % ("\t".join(record[1:4]), stringSummary) 

    def ProcessString(self, string):
        for i in ["$"] + readStartPattern.findall(string):
            string = string.replace(i, "")
        indelMatches = []
        for match in (indelPattern.findall(string)):
            match = list(match)
            match[2] = match[2][:int(match[1])]
            match = "".join(match)
            indelMatches.append(match)
            string = string.replace(match, "")
        return list(string) + indelMatches

    def EnumerateBases(self, stringData):
        baseFrequency = {}
        for base in stringData:
            try:
                baseFrequency[base.upper()] += 1
            except KeyError:
                baseFrequency[base.upper()] = 1
        return "\t".join("%s:%i" % (key, value) for key, value in baseFrequency.items())


pileupParser = PileupParser()
with open(sys.argv[2], "w") as outputHandle:
    for line in open(sys.argv[1], "rU").readlines():
        outputHandle.write(pileupParser.ProcessRecord(line))