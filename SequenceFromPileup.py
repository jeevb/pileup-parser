##########################################################################################
# SYNTAX: python PileupParserExtended.py pileup_input_file output_file                   #
##########################################################################################

import os, re, sys, operator

##########################################################################################
# C O N F I G U R A T I O N                                                              #
##########################################################################################
readStartPattern            = re.compile("(\^.)")
indelPattern                = re.compile("([\+-])([\d]+)([ACGTNacgtn]+)")
START_COORD                 = 695
END_COORD                   = 1693
##########################################################################################

class PileupParser():

    def ProcessRecord(self, _record):
        record = _record.strip().split("\t")
        if START_COORD and int(record[1]) < START_COORD:
            return ""
        if END_COORD and int(record[1]) > END_COORD:
            return ""
        consensusBase = self.EnumerateBases(self.ProcessString(record[4]))
        if consensusBase == ".":
            return record[2]
        elif consensusBase == "*":
            return ""
        else:
            return consensusBase

    def ProcessString(self, string):
        for i in ["$"] + readStartPattern.findall(string):
            string = string.replace(i, "")
        for match in (indelPattern.findall(string)):
            match = list(match)
            match[2] = match[2][:int(match[1])]
            string = string.replace("".join(match), "")
        string = string.replace(",", ".")
        return string

    def EnumerateBases(self, string):
        baseFrequency = {}
        for base in string:
            try:
                baseFrequency[base.upper()] += 1
            except KeyError:
                baseFrequency[base.upper()] = 1
        baseFrequency = sorted(baseFrequency.iteritems(), key = operator.itemgetter(1), reverse = True)
        return baseFrequency[0][0]


pileupParser = PileupParser()
with open(sys.argv[2], "w") as outputHandle:
    for line in open(sys.argv[1], "rU").readlines():
        outputHandle.write(pileupParser.ProcessRecord(line))