##########################################################################################
# SYNTAX: python PileupParserExtended.py pileup_input_file output_file                   #
##########################################################################################

import os, re, sys
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot

##########################################################################################
# C O N F I G U R A T I O N                                                              #
##########################################################################################
readStartPattern            = re.compile("(\^.)")
indelPattern                = re.compile("([\+-])([\d]+)([ACGTNacgtn]+)")
START_COORD                 = 1940
END_COORD                   = 2374
COUNT_SHIFT_INDELS_ONLY     = True
ERROR_FREQUENCY_THRESHOLD   = 0
##########################################################################################

class PileupParser():
    def __init__(self):
        self.errorFreq = {}

    def ProcessRecord(self, _record):
        record = _record.strip().split("\t")
        if START_COORD and int(record[1]) < START_COORD:
            return ""
        if END_COORD and int(record[1]) > END_COORD:
            return ""
        errorFreq, stringSummary = self.EnumerateBases(self.ProcessString(record[4]))
        if ERROR_FREQUENCY_THRESHOLD and errorFreq < ERROR_FREQUENCY_THRESHOLD:
            return ""
        try:
            self.errorFreq[int(record[1])]
        except KeyError:
            self.errorFreq[int(record[1])] = errorFreq
        else:
            assert False, "Duplicate base index encountered: %s" % record[1]
        return "%s\t%s\n" % ("\t".join(record[1:4]), stringSummary) 

    def ProcessString(self, string):
        for i in ["$"] + readStartPattern.findall(string):
            string = string.replace(i, "")
        for match in (indelPattern.findall(string)):
            match = list(match)
            match[2] = match[2][:int(match[1])]
            if int(match[1]) % 3: # Frameshift-causing indels
                string = string.replace("".join(match), match[0])
            else: # regular indels
                if match[0] == "+":
                    string = string.replace("".join(match), "I")
                elif match[0] == "-":
                    string = string.replace("".join(match), "D")
        return string

    def EnumerateBases(self, string):
        baseFrequency = {}
        totalCounter = 0
        incorrectCounter = 0
        for base in string:
            totalCounter += 1
            if base in [".", ","]:
                continue
            if COUNT_SHIFT_INDELS_ONLY and base not in ["*", "+", "-"]:
                continue
            incorrectCounter += 1
            try:
                baseFrequency[base.upper()] += 1
            except KeyError:
                baseFrequency[base.upper()] = 1
        errorFreq = float(incorrectCounter) / float(totalCounter)
        return errorFreq, "%.3f\t%s" % (
            errorFreq,
            "\t".join("%s:%i" % (key, value) for key, value in baseFrequency.items())
        )

    def PlotData(self, path):
        self.GenerateGraph(self.errorFreq.keys(), self.errorFreq.values(), path, yAxisLabel = "Error Frequency")

    def GenerateGraph(self, xValues, yValues, fileName, barColor = None, errorBarValues = None, yAxisLabel = None, xValueLabels = None):
            pyplot.figure(figsize = (16.5, 11.7)) # US A3
            pyplot.bar(xValues, yValues, color=barColor, align = "center", yerr = errorBarValues)
            if yAxisLabel:
                pyplot.ylabel(yAxisLabel)
            if xValueLabels:
                pyplot.xticks(xValues, xValueLabels, size = "small", rotation = 90)
            pyplot.savefig(fileName)
            pyplot.close()


pileupParser = PileupParser()
with open(sys.argv[2], "w") as outputHandle:
    for line in open(sys.argv[1], "rU").readlines():
        outputHandle.write(pileupParser.ProcessRecord(line))

pileupParser.PlotData("%s.png" % os.path.splitext(sys.argv[2])[0])